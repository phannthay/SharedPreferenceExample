package com.example.phann.sharedpreferences1;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText name,email;
    Button save,clear,retrieve;
    SharedPreferences sharedPreferences;
    TextView tvName,tvEmail;
    public static final String mypreferences = "myPref";
    public static final String Name = "nameKey";
    public static final String Email = "emailKey";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.txtname);
        email = findViewById(R.id.txtemail);
        save = findViewById(R.id.btsave);
        clear = findViewById(R.id.btClear);
        retrieve = findViewById(R.id.btRetriev);

        sharedPreferences = getSharedPreferences(Name, Context.MODE_PRIVATE);
        if (sharedPreferences.contains(Name)){
            name.setText(sharedPreferences.getString(Name," "));
        }
        if (sharedPreferences.contains(Email)){
            email.setText(sharedPreferences.getString(Email," "));
        }
    }
    public void Save(View view){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String n = name.getText().toString();
        String e = email.getText().toString();
        editor.putString(Name,n);
        editor.putString(Email,e);
        editor.commit();
    }
    public void Clear(View view){
        name.setText(" ");
        email.setText(" ");
    }
    public void Retrieve(View view){
        tvName = findViewById(R.id.tname);
        tvEmail = findViewById(R.id.temail);
        sharedPreferences = getSharedPreferences(mypreferences,Context.MODE_PRIVATE);
        if (sharedPreferences.contains(Name)){
            tvName.setText(sharedPreferences.getString(Name,""));
        }
        if (sharedPreferences.contains(Email)){
            tvEmail.setText(sharedPreferences.getString(Email," "));
        }
        Toast.makeText(this, "saved", Toast.LENGTH_SHORT).show();
    }
}
